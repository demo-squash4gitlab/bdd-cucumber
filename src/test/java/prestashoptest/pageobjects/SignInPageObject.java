package prestashoptest.pageobjects;

import prestashoptest.seleniumtools.PageObjectBase;

/**
 * Sign In Page
 */
public class SignInPageObject extends PageObjectBase {

    private static final Selector FIELD_EMAIL = new Selector(SelectBy.ID, "field-email");
    private static final Selector FIELD_PASSWORD = new Selector(SelectBy.ID, "field-password");
    private static final Selector BUTTON_SIGNIN = new Selector(SelectBy.ID, "submit-login");

    public SignInPageObject() {
        super("login");
    }

    /**
     * Initiate the login with the specified customer mail and password
     *
     * @param email    Email to fill
     * @param password Password to fill
     */
    public void signIn(final String email,
                       final String password) {
        fillEmail(email);
        fillPassword(password);
        submitForm();
    }

    /**
     * Fill the mail form field
     *
     * @param email Email to fill
     */
    public void fillEmail(final String email) {
        fillFieldValue(FIELD_EMAIL, email);
    }

    /**
     * Fill the password form field
     *
     * @param password Password to fill
     */
    public void fillPassword(final String password) {
        fillFieldValue(FIELD_PASSWORD, password);
    }

    /**
     * Submit the form
     */
    public void submitForm() {
        clickElement(BUTTON_SIGNIN);
    }
}
