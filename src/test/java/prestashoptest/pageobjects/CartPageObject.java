package prestashoptest.pageobjects;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import prestashoptest.datatypes.CartAndOrderLine;
import prestashoptest.helpers.FloatsHelper;
import prestashoptest.seleniumtools.PageObjectBase;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Cart page
 */
public class CartPageObject extends PageObjectBase {

    private static final Selector CART_ITEM = new Selector(SelectBy.CLASS, "cart-item");
    private static final Selector BUTTON_ORDER = new Selector(SelectBy.XPATH, "//*[contains(@class,'checkout')]//a[contains(@class,'btn')]");
    private static final Selector TOTAL_QUANTITY = new Selector(SelectBy.XPATH, "//*[@id='cart-subtotal-products']//span[contains(@class,'subtotal')]");
    private static final Selector TOTAL_PRICE = new Selector(SelectBy.XPATH, "//*[@id='cart-subtotal-products']//span[@class='value']");
    private static final Selector BUTTON_CLOSE_CUSTOMIZATION_MODAL = new Selector(SelectBy.XPATH, "//div[contains(@id,'product-customizations-modal')]//button[@class='close']");
    private static final Selector CUSTOMISATION_TEXT = new Selector(SelectBy.XPATH, "//div[contains(@class, 'product-customization-line')]/div[contains(@class, 'value')]");

    public CartPageObject() {
        super("cart?action=show");
    }

    /**
     * return the content of the cart
     *
     * @return List of content
     * @throws ParseException The prices are not formatted correctly
     */
    public List<CartAndOrderLine> getContent() {
        final List<WebElement> items = getElements(CART_ITEM);
        final List<CartAndOrderLine> list = new ArrayList<>(items.size());
        for (final WebElement item : items) {
            final WebElement productElem = item.findElement(By.className("label"));
            final String product = productElem.getText();
            final WebElement quantityElem = item.findElement(By.name("product-quantity-spin"));
            final int quantity = Integer.parseInt(quantityElem.getAttribute("value"));
            final List<WebElement> dimensionElem = item.findElements(By.className("dimension"));
            final String dimension = dimensionElem.isEmpty() ? null : dimensionElem.get(0).findElement(By.className("value")).getText();
            final List<WebElement> sizeElem = item.findElements(By.className("size"));
            final String size = sizeElem.isEmpty() ? null : sizeElem.get(0).findElement(By.className("value")).getText();
            final List<WebElement> colorElem = item.findElements(By.className("color"));
            final String color = colorElem.isEmpty() ? null : colorElem.get(0).findElement(By.className("value")).getText();
            final List<WebElement> openCustomizationModal = item.findElements(By.xpath(".//a[@data-toggle='modal']"));
            String customization = null;
            if (!openCustomizationModal.isEmpty()) {
                openCustomizationModal.get(0).click();
                waitElementIsClickable(BUTTON_CLOSE_CUSTOMIZATION_MODAL); // wait that the modal dialog is properly displayed
                customization = getElementText(CUSTOMISATION_TEXT);
                clickElement(BUTTON_CLOSE_CUSTOMIZATION_MODAL);
            }
            final WebElement priceElem = item.findElement(By.className("price"));
            final WebElement totalProductPriceElem = item.findElement(By.xpath(".//span[@class='product-price']"));
            Float productPrice = null;
            Float totalProductPrice = null;
            try {
                productPrice = FloatsHelper.convertPriceStringToFloat(priceElem.getText());
                totalProductPrice = FloatsHelper.convertPriceStringToFloat(totalProductPriceElem.getText());
            } catch (final ParseException e) {
                Assertions.fail("The prices are not formatted correctly:" + e.getMessage());
            }
            list.add(new CartAndOrderLine(product, quantity, dimension, size, color, customization, productPrice, totalProductPrice));
        }
        return list;
    }

    /**
     * Get product names as a list
     *
     * @param cartAndOrderLines List which contains the information defined in the keyword
     * @return List of product names
     */
    public List<String> getProductNames(final List<CartAndOrderLine> cartAndOrderLines) {
        return cartAndOrderLines.stream()
                .map(CartAndOrderLine::getProduct)
                .collect(Collectors.toList());
    }

    /**
     * Get product quantity as a list
     *
     * @param cartAndOrderLines List which contains the information defined in the keyword
     * @return List of quantity product
     */
    public List<Integer> getQuantities(final List<CartAndOrderLine> cartAndOrderLines) {
        return cartAndOrderLines.stream()
                .map(CartAndOrderLine::getQuantity)
                .collect(Collectors.toList());
    }

    /**
     * Fill in a quantity for the products on the cart
     *
     * @param selector Field to fill
     * @param quantity Quantity filled
     */
    public void fillQuantity(final Selector selector,
                             final int quantity) {
        fillFieldValue(selector, String.valueOf(quantity));
        final WebElement element = getElement(selector);
        element.sendKeys(Keys.ENTER);
    }

    /**
     * Update quantity for the products on the cart
     *
     * @param productName Name of the product
     * @param quantity    Quantity to update
     */
    public void updateQuantity(final String productName,
                               final int quantity) {
        final String quantityDynamicXpath = "//a[contains(text(), '" + productName + "')]/ancestor::div[contains(@class,'product-line')]" +
                                            "//input[contains(@class,'product-quantity')]";
        final Selector quantitySelector = new Selector(SelectBy.XPATH, quantityDynamicXpath);
        fillQuantity(quantitySelector, quantity);
    }

    /**
     * Suppress a product in the cart
     *
     * @param selector Button to suppress a specific product
     */
    public void suppressCartProduct(final Selector selector) {
        clickElement(selector);
    }

    /**
     * Initiate placing order process
     */
    public void order() {
        clickElement(BUTTON_ORDER);
    }

    /**
     * Get the total products count
     *
     * @return Total of products count
     */
    public int getTotalProductQuantity() {
        //TOTAL_QUANTITY text contains alphanumeric characters, so we must use a regex to make it an "int"
        return Integer.parseInt(getElementText(TOTAL_QUANTITY).replaceAll("[^0-9]", ""));
    }

    /**
     * Get the total price
     *
     * @return Total of products price
     */
    public float getTotalProductPrice() {
        try {
            return FloatsHelper.convertStringToFloat(getElementText(TOTAL_PRICE));
        } catch (final ParseException e) {
            Assertions.fail("Price is improperly formatted: " + e.getMessage());
            return 0.0f; // cannot occur, necessary to compile
        }
    }

    /**
     * Remove a product of the cart
     *
     * @param product Name of product to remove
     */
    public void removeProduct(final String product) {
        final String productDynamicXpath = "//a[contains(text(), '" + product + "')]/ancestor::div[contains(@class,'product-line')]" +
                "//a[@class='remove-from-cart']";
        final Selector productSelector = new Selector(SelectBy.XPATH, productDynamicXpath);
        suppressCartProduct(productSelector);
    }
}