package prestashoptest.pageobjects;

import org.junit.Assert;
import prestashoptest.datatypes.Gender;
import prestashoptest.seleniumtools.PageObjectBase;

import java.time.LocalDate;

/**
 * Account creation page
 */
public class AccountCreationPageObject extends PageObjectBase {

    private static final Selector RADIO_ID_MALE = new Selector(SelectBy.ID, "field-id_gender-1");
    private static final Selector RADIO_ID_FEMALE = new Selector(SelectBy.ID, "field-id_gender-2");
    private static final Selector FIELD_FIRST_NAME = new Selector(SelectBy.ID, "field-firstname");
    private static final Selector FIELD_LAST_NAME = new Selector(SelectBy.ID, "field-lastname");
    private static final Selector FIELD_EMAIL = new Selector(SelectBy.ID, "field-email");
    private static final Selector FIELD_PASSWORD = new Selector(SelectBy.ID, "field-password");
    private static final Selector FIELD_BIRTHDAY = new Selector(SelectBy.ID, "field-birthday");
    private static final Selector CB_PARTNER_OFFERS = new Selector(SelectBy.NAME, "optin");
    private static final Selector CB_PRIVACY_POLICY = new Selector(SelectBy.NAME, "customer_privacy");
    private static final Selector CB_NEWSLETTER = new Selector(SelectBy.NAME, "newsletter");
    private static final Selector CB_GDPR = new Selector(SelectBy.NAME, "psgdpr");
    private static final Selector BUTTON_SUBMIT = new Selector(SelectBy.XPATH, "//*[@id=\"customer-form\"]//button[@type=\"submit\"] ");

    public AccountCreationPageObject() {
        super("login?create_account=1");
    }

    /**
     * Fill the form with the specified values and initiate the account creation.
     *
     * @param gender              The gender to select
     * @param firstName           The first name to fill
     * @param lastName            The last name to fill
     * @param password            The initial password to use
     * @param email               The email address to fill
     * @param birthDate           The birthdate to fill in the format "yyyy-MM-dd"
     * @param acceptPartnerOffers boolean to indicate whether to accept partner offers
     * @param acceptPrivacyPolicy boolean to indicate whether to accept the privacy policy
     * @param acceptNewsletter    boolean to indicate whether to accept newsletters
     * @param acceptGdpr          boolean to indicate whether to accept GDPR terms
     */
    public void fillNewAccountFields(final Gender gender,
                                     final String firstName,
                                     final String lastName,
                                     final String password,
                                     final String email,
                                     final LocalDate birthDate,
                                     final boolean acceptPartnerOffers,
                                     final boolean acceptPrivacyPolicy,
                                     final boolean acceptNewsletter,
                                     final boolean acceptGdpr) {
        fillGender(gender);
        fillFirstName(firstName);
        fillLastName(lastName);
        fillEmail(email);
        fillPassword(password);
        fillBirthDate(birthDate);
        if (acceptPartnerOffers) acceptPartnerOffers();
        if (acceptPrivacyPolicy) acceptPrivacyPolicy();
        if (acceptNewsletter) acceptNewsletter();
        if (acceptGdpr) acceptGdpr();
        submitNewAccountForm();
    }

    /**
     * Fill the gender field
     * If the gender is undefined, the field is untouched.
     *
     * @param gender Gender to fill
     */
    public void fillGender(final Gender gender) {
        if (gender.equals(Gender.UNDEFINED)) {
            return;
        }
        final Selector genderField = (gender.equals(Gender.MALE)) ? RADIO_ID_MALE
                : RADIO_ID_FEMALE;
        clickElement(genderField);
    }

    /**
     * Fill the first name field
     *
     * @param firstName First name to fill
     */
    public void fillFirstName(final String firstName) {
        fillFieldValue(FIELD_FIRST_NAME, firstName);
    }

    /**
     * Fill the last name field
     *
     * @param lastName Last name to fill
     */
    public void fillLastName(final String lastName) {
        fillFieldValue(FIELD_LAST_NAME, lastName);
    }

    /**
     * Fill the email field
     *
     * @param email Email to fill
     */
    public void fillEmail(final String email) {
        fillFieldValue(FIELD_EMAIL, email);
    }

    /**
     * Fill the password field
     *
     * @param password Password to fill
     */
    public void fillPassword(final String password) {
        fillFieldValue(FIELD_PASSWORD, password);
    }

    /**
     * Fill the birthdate field
     *
     * @param birthDate Birthdate to fill
     */
    public void fillBirthDate(final LocalDate birthDate) {
        fillFieldValue(FIELD_BIRTHDAY, birthDate.format(getDateFormatter()));
    }

    /**
     * Approve for partner offers
     */
    public void acceptPartnerOffers() { clickElement(CB_PARTNER_OFFERS);}

    /**
     * Approve the customer privacy policy
     */
    public void acceptPrivacyPolicy() {
        clickElement(CB_PRIVACY_POLICY);
    }

    /**
     * Sign up for the newsletter
     */
    public void acceptNewsletter() {
        clickElement(CB_NEWSLETTER);
    }

    /**
     * Approve the GDPR policy
     */
    public void acceptGdpr() {
        clickElement(CB_GDPR);
    }

    /**
     * Initiate the account creation
     */
    public void submitNewAccountForm() {
        clickElement(BUTTON_SUBMIT);
    }

    public void getErrorMessage(final String field) {
        //Map of the different choice that the customer can use in his test
        Selector xpath;
        switch (field) {
            case "email_txt":
                xpath = FIELD_EMAIL;
                break;
            case "privacy_cb":
                xpath = CB_PRIVACY_POLICY;
                break;
            case "gdpr_cb":
                xpath = CB_GDPR;
                break;
            case "password_txt":
                xpath = FIELD_PASSWORD;
                break;
            default:
                throw new IllegalStateException(field + "is not a valid choice , please choose between : email_txt , privacy_cb , gdpr_cb , password_txt");
        }
        Assert.assertNotNull(getElementAttribute(xpath, "validationMessage"));
    }
}