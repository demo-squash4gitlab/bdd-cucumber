package prestashoptest.pageobjects;

import prestashoptest.seleniumtools.PageObjectBase;

import java.util.regex.Pattern;

/**
 * Product page
 */
public class ProductPageObject extends PageObjectBase {

    private static final Selector ICON_QUANTITY_WARNING = new Selector(SelectBy.XPATH, "//span[contains(@id,'product-availability')]//i[contains(@class,'product-unavailable')]");
    private static final Selector MESSAGE_QUANTITY_WARNING = new Selector(SelectBy.ID, "product-availability");
    private static final Selector FIELD_CUSTOMIZE_MESSAGE = new Selector(SelectBy.ID, "field-textField1");
    private static final Selector FIELD_UPDATE_QUANTITY = new Selector(SelectBy.ID, "quantity_wanted");
    private static final Selector BUTTON_ADD_TO_CART = new Selector(SelectBy.XPATH, "//button[contains(@class,'add-to-cart')]");
    private static final Selector BUTTON_CUSTOMIZE_MESSAGE = new Selector(SelectBy.NAME, "submitCustomizedData");
    private static final Selector BUTTON_CLOSE_COMMAND_HEADER = new Selector(SelectBy.XPATH, "//div[@id='blockcart-modal']//button[@class='close']");
    private static final Selector MODAL_LABEL = new Selector(SelectBy.ID, "myModalLabel");
    private static final Selector MESSAGE_CUSTOM = new Selector(SelectBy.XPATH, "//*[@class='customization-message']//label");
    private static final Selector MODAL_FADER_DIV = new Selector(SelectBy.XPATH, "//div[@id='blockcart-modal']/following-sibling::div[contains(@class,'modal')]");

    public ProductPageObject() {
        super(Pattern.compile( "[-\\w]+/\\d+-(\\d+-)?[-\\w]+\\.html(#.*)?"));
    }

    /**
     * Add one instance of the product to the cart
     */
    public void addToCart() {
        clickElement(BUTTON_ADD_TO_CART);
        waitElementIsVisible(MODAL_LABEL);
        clickElement(BUTTON_CLOSE_COMMAND_HEADER);
        //The modal should disappear, but the fade animation is slower.
        //We need to wait for the animation to complete before continuing.
        waitElementIsInvisible(MODAL_FADER_DIV);
    }

    /**
     * Add message
     *
     * @param message message to add
     */
    public void addMessage(final String message) {
        fillFieldValue(FIELD_CUSTOMIZE_MESSAGE, message);
        clickElement(BUTTON_CUSTOMIZE_MESSAGE);
    }

    /**
     * Update quantity order
     *
     * @param quantity Quantity to fill
     */
    public void fillQuantity(final int quantity) {
        fillFieldValue(FIELD_UPDATE_QUANTITY, String.valueOf(quantity));
    }

    /**
     * Get quantity warning message
     *
     * @return warning message
     */
    public String getQuantityWarningMessage() {
        waitElementIsVisible(ICON_QUANTITY_WARNING);
        return getElementOnlyText(MESSAGE_QUANTITY_WARNING).strip();
    }

    /**
     * Get product customization message
     *
     * @return product customization message
     */
    public String getCustomMessage() {
        return getElementText(MESSAGE_CUSTOM);
    }

    /**
     * Get the state of the add to cart button
     *
     * @return state of the add to cart button
     */
    public boolean isAddToCartPossible() {
        return isButtonClickable(BUTTON_ADD_TO_CART);
    }
}
