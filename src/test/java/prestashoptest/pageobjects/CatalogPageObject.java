package prestashoptest.pageobjects;

import prestashoptest.seleniumtools.PageObjectBase;

import java.util.regex.Pattern;

/**
 * Catalog page
 */
public class CatalogPageObject extends PageObjectBase {

    public CatalogPageObject() {
        super(Pattern.compile("\\d+-\\w+"));
    }

    /**
     * Select a product in the catalog
     *
     * @param product Name of product to select
     */
    public void selectProduct(final String product) {
        // (this will not behave properly if two or more products are sharing the same truncated name)
        final Selector productSelector = new Selector(SelectBy.XPATH, "//a[text()='" + truncate(product) + "']");
        waitElementIsClickable(productSelector);
        clickElement(productSelector);
    }

    /**
     * PrestaShop truncates the product names, we need to do the same
     *
     * @param str Product name
     * @return Truncated product name
     */
    private static String truncate(String str) {
        while (str.length() > 30) {
            str = str.replaceAll(" [^ ]+$", "...");
        }
        return str;

        /* here is a unit test to ensure that this method truncates the same way as PrestaShop
         *     @Test
            void test() {
                Assertions.assertEquals("Affiche encadrée The best...", Truc.truncate("Affiche encadrée The best is yet to come"));
                Assertions.assertEquals("Affiche encadrée The...", Truc.truncate("Affiche encadrée The adventure begins"));
                Assertions.assertEquals("Affiche encadrée Today is a...", Truc.truncate("Affiche encadrée Today is a good day"));
                Assertions.assertEquals("Illustration vectorielle...", Truc.truncate("Illustration vectorielle Renard"));
                Assertions.assertEquals("Illustration vectorielle...", Truc.truncate("Illustration vectorielle Colibri"));
                Assertions.assertEquals("Illustration vectorielle...", Truc.truncate("Illustration vectorielle Ours brun"));
                Assertions.assertEquals("Pack Mug + Affiche encadrée", Truc.truncate("Pack Mug + Affiche encadrée"));
                Assertions.assertEquals("Carnet de notes Renard", Truc.truncate("Carnet de notes Renard"));
                Assertions.assertEquals("Carnet de notes Ours brun", Truc.truncate("Carnet de notes Ours brun"));
                Assertions.assertEquals("Carnet de notes Colibri", Truc.truncate("Carnet de notes Colibri"));
            }
        */
    }
}
