package prestashoptest;

import io.cucumber.java.After;
import io.cucumber.java.AfterStep;
import io.cucumber.java.Before;
import io.cucumber.java.BeforeStep;
import io.cucumber.java.Scenario;
import org.junit.Assert;
import org.opentestfactory.exception.ParameterException;
import org.opentestfactory.util.ParameterService;
import prestashoptest.datatypes.Customer;
import prestashoptest.helpers.ApiHelper;
import prestashoptest.helpers.Logger;
import prestashoptest.seleniumtools.ComponentObjectBase;
import prestashoptest.seleniumtools.PageObjectBase;

import java.util.Locale;

public class SetupTeardown {

    private static Customer customerToBeDeleted;
    private static Logger logger;
    private static String orderToBeDeleted;

    /**
     * The order reference will be deleted during the teardown phase.<br>
     * We must use this method to delete after performing a new order.<br>
     * Currently, we can only record one order.
     *
     * @param reference
     */
    public static void recordOrderToBeDeleted(final String reference) {
        if (orderToBeDeleted != null) {
            throw new IllegalStateException("recordOrderToBeDeleted cannot be called twice");
        }
        orderToBeDeleted = reference;
    }

    /**
     * @return Logger
     */
    public static Logger getLogger() {
        return logger;
    }

    /**
     * test setup
     */
    @Before
    public static void setup() throws ParameterException {
        Locale.setDefault(Locale.FRENCH);
        logger = new Logger();

        logger.logSetupStart();

        final String browser = System.getenv("PRESTA_BROWSER_TYPE");
        if (browser == null) {
            logger.logSetupEnd();
            Assert.fail("The PRESTA_BROWSER_TYPE environment variable is not set");
        }
        try {
            ComponentObjectBase.setBrowser(convertStringToBrowser(browser));
        } catch (final IllegalStateException e) {
            logger.logSetupEnd();
            throw e;
        }

        String url = null;
        try {
            url = ParameterService.INSTANCE.getString("IT_CUF_presta_url");
        } catch (final ParameterException e) {
            logger.logSetupEnd();
            throw e;
        }
        ComponentObjectBase.setHost(url);

        ApiHelper.setUrl(url);
        ApiHelper.setLogger(logger);

        orderToBeDeleted = null;
        customerToBeDeleted = null;

        logger.logSetupEnd();
    }

    /**
     *
     * test teardown
     *
     * @param scenario
     */
    @After
    public static void teardown(final Scenario scenario) {

        logger.logTeardownStart();

        // create snapshot in case of a test failure
        if (scenario.isFailed()) {
            final byte[] screenshot = PageObjectBase.getScreenshot();
            if (screenshot != null) {
                final String screenshotName = "screenshot " + scenario.getName() + " (line " + scenario.getLine() + ")";
                scenario.attach(screenshot, "image/png", screenshotName);
            }
        }
        ComponentObjectBase.quit();

        deleteOrder();
        deleteCustomer();

        logger.logTeardownEnd();

        final String logs = logger.getLogs();
        if (logs.length() > 0) {
            scenario.log(logs);
        }
    }

    /**
     * Hook before step
     *
     * @param scenario
     */
    @BeforeStep
    public static void beforeStep(final Scenario scenario) {
        logger.logStepStart();
    }

    /**
     * Hook after step
     *
     * @param scenario
     */
    @AfterStep
    public static void afterStep(final Scenario scenario) {
        logger.logStepEnd();
    }

    /**
     * Set the Customer to be deleted at the end of the test
     *
     * @param firstName
     * @param lastName
     * @param email
     * @param password
     */
    public static void recordCustomerToBeDeleted(final String firstName,
                                                 final String lastName,
                                                 final String email,
                                                 final String password) {
        if (customerToBeDeleted != null) {
            throw new IllegalStateException(" recordCustomerToBeDeleted cannot be called twice");
        }

        customerToBeDeleted = new Customer(firstName, lastName, email, password);
    }

    /**
     * Replace the customer to be deleted at the end of the test
     *
     * @param firstName
     * @param lastName
     * @param email
     * @param password
     */
    public static void replaceCustomerToBeDeleted(final String firstName,
                                                  final String lastName,
                                                  final String email,
                                                  final String password) {
        if (customerToBeDeleted == null) {
            throw new IllegalStateException("replaceCustomerToBeDeleted is called while there is currently no Customer to be deleted");
        }

        customerToBeDeleted = new Customer(firstName, lastName, email, password);
    }

    /**
     * @return Customer to be deleted at the end of the test
     */
    public static Customer getCustomerToBeDeleted() {
        return customerToBeDeleted;
    }

    private static ComponentObjectBase.Browser convertStringToBrowser(final String browser) {
        switch (browser) {
            case "chrome":
                return ComponentObjectBase.Browser.CHROME;
            case "firefox":
                return ComponentObjectBase.Browser.FIREFOX;
            default:
                throw new IllegalStateException("'" + browser + "' is an invalid browser (should be 'chrome' or 'firefox')");
        }
    }

    /**
     * Delete a customer by making an API call from the Api class.
     * It uses the variable customerToBeDeleted to interact with the current Customer set in the Customer class.
     */
    private static void deleteCustomer() {
        if (customerToBeDeleted == null) {
            // no customer to be deleted
            return;
        }

        ApiHelper.deleteCustomer(customerToBeDeleted.getEmail());
        customerToBeDeleted = null;
    }

    /**
     * Delete the order by making an API call from the Api class.
     * It utilizes the variable orderToBeDeleted to obtain the unique order reference and perform the deletion.
     */
    private static void deleteOrder() {
        if (orderToBeDeleted == null) {
            //no order to be deleted
            return;
        }

        ApiHelper.deleteOrder(orderToBeDeleted);
        orderToBeDeleted = null;
    }
}
