package prestashoptest.stepdefinitions;

import io.cucumber.datatable.DataTable;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import prestashoptest.datatypes.CartAndOrderLine;
import prestashoptest.helpers.FloatsHelper;
import prestashoptest.helpers.OrderContentVerification;
import prestashoptest.helpers.StringsHelper;
import prestashoptest.pageobjects.CartPageObject;
import prestashoptest.pageobjects.OrderPageObject;

import java.text.ParseException;
import java.util.List;

public class OrderStepDefinitions {

    /**
     * Parse a string which may be between double quotes.
     * If it is between double quotes, the quotes are removed.
     *
     * @param input string
     * @return cleaned string
     */
    @ParameterType(".+|\".*\"")
    public String quotableString(final String input) {
        if (input.startsWith("\"") && input.endsWith("\"")) {
            return input.substring(1, input.length()-1);
        }
        return input;
    }

    /**
     * Parse an integer which may be between double quotes.
     *
     * @param input string
     * @return cleaned integer
     */
    @ParameterType("\\d+|\"\\d+\"")
    public int quotableInteger(final String input) {
        return Integer.parseInt(quotableString(input));
    }

    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ACT-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-//

    /**
     * Initiate placing order process
     */
    @When("I initiate order placement process")
    public void initiateOrderProcess() {
        final CartPageObject cartPage = new CartPageObject();
        cartPage.assertIsCurrent();
        cartPage.order();
    }

    /**
     * Fill login form and connect
     */
    @When("I fill login form with email {string} and password {string}")
    public void fillLoginForm(final String email,
                              final String password) {
        final OrderPageObject orderPage = new OrderPageObject();
        orderPage.goToLoginForm();
        orderPage.fillEmail(email);
        orderPage.fillPassword(password);
        orderPage.submitConnection();
    }

    /**
     * Fill command form
     * @param alias
     * @param company
     * @param vat
     * @param address
     * @param supp
     * @param zip
     * @param city
     * @param country
     * @param phone
     * @param facturation
     */
    @When("I fill command form with alias {string} company {string} vat {quotableString} address {string} supp {string} zip {quotableInteger} city {string} country {string} phone {quotableString} and facturation {string} and submit")
    public void fillCommandForm(final String alias,
                                final String company,
                                final String vat,
                                final String address,
                                final String supp,
                                final int zip,
                                final String city,
                                final String country,
                                final String phone,
                                final String facturation) {
        final OrderPageObject orderPage = new OrderPageObject();
        orderPage.assertIsCurrent();
        orderPage.fillFormToOrder(alias, company, vat, address, supp, zip, city, phone);
        orderPage.chooseCountry(country);
        orderPage.clickCheckBoxFacturationIfNecessary(StringsHelper.convertYesNoIntoBoolean(facturation));
        orderPage.submitAddressForm();
        orderPage.assertValidAddressForm();
    }

    /**
     * Fill delivery form
     * @param delivery
     * @param deliveryMessage
     */
    @When("I choose delivery {string} and command message {string}")
    public void fillDeliveryAndCommandMessage(final String delivery,
                                              final String deliveryMessage) {
        final OrderPageObject orderPage = new OrderPageObject();
        orderPage.chooseDelivery(delivery);
        orderPage.fillDeliveryMessage(deliveryMessage);
        orderPage.submitDeliveryForm();
    }

    /**
     * Choose paymode and approve sales conditions
     * @param paymode
     * @param approveSalesConditions
     */
    @When("I pay by paymode {string} and choose approveSalesConditions {string}")
    public void selectPaymodeAndApproveSalesConditions(final String paymode,
                                                       final String approveSalesConditions) {
        final OrderPageObject orderPage = new OrderPageObject();
        orderPage.choosePaymode(paymode);
        orderPage.clickCheckBoxConditionIfNecessary(StringsHelper.convertYesNoIntoBoolean(approveSalesConditions));
    }

    /**
     * Submit order
     */
    @When("I submit order")
    public void submitOrder() {
        final OrderPageObject orderPage = new OrderPageObject();
        orderPage.submitOrder();
        orderPage.setOrderToBeDeleted();
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ACT-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-//
    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ASSERT-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓//

    /**
     * Check if the Submit button is enabled
     */
    @Then("The submit order button should be disabled")
    public void assertSubmitOrderButtonIsDisabled() {
        final OrderPageObject orderPage = new OrderPageObject();
        Assertions.assertFalse(orderPage.isOrderPossible());
    }

    /**
     * Check the cart
     * @param order order content (see {@link OrderContentVerification#extractCartAndOrderLines})
     */
    @Then("The order should be placed and it should contain")
    public void assertOrderContent(final DataTable order) {
        final OrderPageObject orderPage = new OrderPageObject();
        final List<CartAndOrderLine> expectedOrderLines = OrderContentVerification.extractCartAndOrderLines(order.asMaps(String.class, String.class));
        final List<CartAndOrderLine> effectiveOrderLines = orderPage.getOrderContent();
        OrderContentVerification.assertEquals(expectedOrderLines, effectiveOrderLines);
    }

    /**
     * Check the total order price
     * @param priceString
     */
    @Then("The total order price should be {word}")
    public void assertTotalOrderPrice(final String priceString) {
        final OrderPageObject orderPage = new OrderPageObject();
        float price = Float.NaN;
        try {
            price = FloatsHelper.convertStringToFloat(priceString);
        } catch (final ParseException e) {
            Assertions.fail("The price is not formatted correctly: " + e.getMessage());
        }
        Assertions.assertEquals(price, orderPage.getTotalPrice(), "The displayed price is different from the expected price");
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ASSERT-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑//
}