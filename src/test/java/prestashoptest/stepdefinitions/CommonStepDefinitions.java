package prestashoptest.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import prestashoptest.pageobjects.AccountCreationPageObject;
import prestashoptest.pageobjects.CartPageObject;
import prestashoptest.pageobjects.HomePageObject;
import prestashoptest.pageobjects.IdentityPageObject;
import prestashoptest.pageobjects.OrderPageObject;
import prestashoptest.pageobjects.SignInPageObject;


public class CommonStepDefinitions {

    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ARRANGE-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-//

    /**
     * Set the current page
     * @param string
     */
    @Given("I am on the {string} page")
    public void setCurrentPage(final String string) {
        switch (string) {
            case "Home":
                new HomePageObject().goTo();
                new HomePageObject().assertIsCurrent();
                break;
            case "AccountCreation":
                new AccountCreationPageObject().goTo();
                new AccountCreationPageObject().assertIsCurrent();
                break;
            case "MyIdentity":
                new IdentityPageObject().goTo();
                new IdentityPageObject().assertIsCurrent();
                break;
            case "Cart":
                new CartPageObject().goTo();
                new CartPageObject().assertIsCurrent();
                break;
            case "Login":
                new SignInPageObject().goTo();
                new SignInPageObject().assertIsCurrent();
                break;
            default:
                throw new IllegalStateException(string + " is an invalid page name: it should be 'AccountCreation', 'MyIdentity', 'Login', 'Cart' and 'Home'");
        }
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ARRANGE-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-//
    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ASSERT-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓//

    /**
     *  Navigate to a page
     * @param string
     */
    @Then("I navigate to the {string} page")
    public void navigateToPage(final String string) {
        switch (string) {
            case "Cart":
                (new CartPageObject()).goTo();
                break;
            case "Login":
                (new SignInPageObject()).goTo();
                break;
            default:
                throw new IllegalStateException(string + " is an invalid page name: it should be 'Cart'and 'Login'");
        }
    }

    /**
     * Check that this is the correct page
     */
    @Then("I should still be on the {string} page")
    public void assertCurrentPage(final String string) {
        switch (string) {
            case "AccountCreation":
                (new AccountCreationPageObject()).assertIsCurrent();
                break;
            case "MyIdentity":
                (new IdentityPageObject()).assertIsCurrent();
                break;
            case "Order":
                (new OrderPageObject()).assertIsCurrent();
                break;
            case "Login":
                (new SignInPageObject()).assertIsCurrent();
                break;
            default:
                throw new IllegalStateException(string + "is an invalid page name: it should be 'AccountCreation', 'MyIdentity', 'Order', and 'Login'");
        }
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ASSERT-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑//
}