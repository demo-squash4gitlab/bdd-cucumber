package prestashoptest.stepdefinitions;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import prestashoptest.SetupTeardown;
import prestashoptest.datatypes.Gender;
import prestashoptest.helpers.ApiHelper;
import prestashoptest.helpers.StringsHelper;
import prestashoptest.pageobjects.AccountCreationPageObject;
import prestashoptest.pageobjects.HeaderObject;
import prestashoptest.pageobjects.HomePageObject;
import prestashoptest.pageobjects.IdentityPageObject;
import prestashoptest.pageobjects.SignInPageObject;
import prestashoptest.seleniumtools.PageObjectBase;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

public class AccountStepDefinitions {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");

     //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ARRANGE-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-//

    /**
     * Create a customer
     * @param gender
     * @param firstName
     * @param lastName
     * @param email
     * @param password
     * @param birthDate
     * @param partnerOffers
     * @param newsletter
     */
    @Given("I created an account with gender {string} firstName {string} lastName {string} email {string} " +
            "password {string} birthDate {string} partnerOffers {string} newsletter {string}")
    public void setNewAccount(final String gender,
                              final String firstName,
                              final String lastName,
                              final String email,
                              final String password,
                              final String birthDate,
                              final String partnerOffers,
                              final String newsletter) {

        ApiHelper.createCustomer(Gender.ofCode(gender),
                firstName,
                lastName,
                email,
                password,
                LocalDate.parse(birthDate, DATE_FORMATTER),
                StringsHelper.convertYesNoIntoBoolean(partnerOffers),
                StringsHelper.convertYesNoIntoBoolean(newsletter));
        SetupTeardown.recordCustomerToBeDeleted(firstName, lastName, email, password);
    }

    /**
     * Create a temporary customer and keep him/her logged in
     */
    @Given("I am logged in")
    public void setTemporaryCustomer() {
        final String id = StringsHelper.generateRandomId();
        final String firstName = "first. " + id;
        final String lastName = "last. " + id;
        final String email = id + "@example.com";
        final String password = id;
        final LocalDate birthDate = LocalDate.of(2000, 1, 1);
        ApiHelper.createCustomer(Gender.UNDEFINED, firstName, lastName, email, password, birthDate, true, true);
        SetupTeardown.recordCustomerToBeDeleted(firstName, lastName, email, password);
        final SignInPageObject signInPageObject = new SignInPageObject();
        signInPageObject.goTo();
        signInPageObject.signIn(email, password);
    }

    /**
     * Log out the customer if necessary
     */
    @Given("I am logged out")
    public void setLogOut() {
        final HomePageObject homePage = new HomePageObject();
        homePage.goTo();
        homePage.assertIsCurrent();
        final HeaderObject headerObject = new HeaderObject();
        if (headerObject.isSignedIn()) {
            headerObject.signOut();
        }
        headerObject.assertConnectionButtonIsVisible();
    }

    /**
     * Log in using the given parameters
     * @param email
     * @param password
     */
    @Given("I am logged in with email {string} and password {string}")
    public void setSignIn(final String email,
                          final String password) {
        final SignInPageObject signInPage = new SignInPageObject();
        signInPage.goTo();
        signInPage.assertIsCurrent();
        signInPage.signIn(email, password);
        new HeaderObject().isSignedIn();
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ARRANGE-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-//
    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ACT-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-//

    /**
     * Fill the fields on the Account Creation page and submit the form
     * <p>
     * The current page must be the Account Creation page
     * @param genderCode
     * @param firstName
     * @param lastName
     * @param password
     * @param email
     * @param birthDate
     * @param acceptPartnerOffers
     * @param acceptPrivacyPolicy
     * @param acceptNewsletter
     * @param acceptGdpr
     */
    @When("I fill AccountCreation fields with gender {string} firstName {string} lastName {string} password {string} " +
            "email {string} birthDate {string} acceptPartnerOffers {string} acceptPrivacyPolicy {string} acceptNewsletter {string} acceptGdpr {string} and submit")
    public void fillAccountCreationFieldsAndSubmit(final String genderCode,
                                                   final String firstName,
                                                   final String lastName,
                                                   final String password,
                                                   final String email,
                                                   final String birthDate,
                                                   final String acceptPartnerOffers,
                                                   final String acceptPrivacyPolicy,
                                                   final String acceptNewsletter,
                                                   final String acceptGdpr) {
        final AccountCreationPageObject newAccountPage = new AccountCreationPageObject();
        newAccountPage.assertIsCurrent();
        newAccountPage.fillNewAccountFields(Gender.ofCode(genderCode),
                firstName,
                lastName,
                password,
                email,
                LocalDate.parse(birthDate, DATE_FORMATTER),
                StringsHelper.convertYesNoIntoBoolean(acceptPartnerOffers),
                StringsHelper.convertYesNoIntoBoolean(acceptPrivacyPolicy),
                StringsHelper.convertYesNoIntoBoolean(acceptNewsletter),
                StringsHelper.convertYesNoIntoBoolean(acceptGdpr));
        SetupTeardown.recordCustomerToBeDeleted(firstName, lastName, email, password);
    }

    /**
     * Log out
     * <p>
     * The current page must be the home page
     */
    @When("I log out")
    public void logOutFromHomePage() {
        final HomePageObject homePage = new HomePageObject();
        homePage.assertIsCurrent();
        final HeaderObject headerObject = new HeaderObject();
        headerObject.signOut();
    }

    /**
     * Fill the fields on the Identity page and submit the form
     * @param gender
     * @param firstName
     * @param lastName
     * @param email
     * @param oldPass
     * @param newPass
     * @param birthDate
     * @param partnerOffers
     * @param privacyPolicy
     * @param newsletter
     * @param gdpr
     */
    @When("I fill MyIdentity fields with gender {string} firstName {string} lastName {string} email {string} oldPass {string}" +
            " newPass {string} birthDate {string} partnerOffers {string} privacyPolicy {string} newsletter {string} gdpr {string} and submit")

    public void fillMyIdentityFields(final String gender,
                                     final String firstName,
                                     final String lastName,
                                     final String email,
                                     final String oldPass,
                                     final String newPass,
                                     final String birthDate,
                                     final String partnerOffers,
                                     final String privacyPolicy,
                                     final String newsletter,
                                     final String gdpr) {
        final IdentityPageObject identityPageObject = new IdentityPageObject();
        identityPageObject.fillIdentityFields(
                Gender.ofCode(gender),
                firstName,
                lastName,
                email,
                oldPass,
                newPass,
                LocalDate.parse(birthDate, DATE_FORMATTER),
                StringsHelper.convertYesNoIntoBoolean(partnerOffers),
                StringsHelper.convertYesNoIntoBoolean(privacyPolicy),
                StringsHelper.convertYesNoIntoBoolean(newsletter),
                StringsHelper.convertYesNoIntoBoolean(gdpr));
    }

    /**
     * Log in
     * @param email
     * @param password
     */
    @When("I log in with email {string} and password {string}")
    public void signInAct(final String email,
                          final String password) {
        final SignInPageObject signInPage = new SignInPageObject();
        signInPage.assertIsCurrent();
        signInPage.signIn(email, password);
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ACT-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-//
    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ASSERT-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓//

    /**
     * Verify that the given message is currently displayed as a success message
     * @param message
     */
    @Then("The message {string} should be displayed")
    public void assertMessageDisplay(final String message) {
        final IdentityPageObject identityPageObject = new IdentityPageObject();
        Assertions.assertEquals(message, identityPageObject.getSuccessMessage(), "The displayed message is different from the expected message");
    }

    /**
     * Verify if the name is visible and if it is the correct name
     * @param name
     */
    @Then("I can see firstName and lastName {string} in the top right corner")
    public void assertNameInTheTopRightCorner(final String name) {
        final HeaderObject headerObject = new HeaderObject();
        Assertions.assertEquals(name, headerObject.getDisplayedCustomerName(), "The displayed name is different from the expected name");
    }

    /**
     * Verify that the personal information is correctly displayed
     * @param genderCode
     * @param firstName
     * @param lastName
     * @param email
     * @param birthDate
     * @param acceptPartnerOffers
     * @param acceptPrivacyPolicy
     * @param acceptNewsletter
     * @param acceptGdpr
     */
    @Then("My personal information should be gender {string} firstName {string} lastName {string} " +
            "email {string} birthDate {string} acceptPartnerOffers {string} acceptPrivacyPolicy {string} acceptNewsletter {string} acceptGdpr {string}")
    public void assertPersonalInformation(final String genderCode,
                                          final String firstName,
                                          final String lastName,
                                          final String email,
                                          final String birthDate,
                                          final String acceptPartnerOffers,
                                          final String acceptPrivacyPolicy,
                                          final String acceptNewsletter,
                                          final String acceptGdpr) {
        final IdentityPageObject identityPage = new IdentityPageObject();
        identityPage.goTo();
        Assertions.assertEquals(Gender.ofCode(genderCode),
                identityPage.getGender(),
                "The effective gender is not the expected one");
        Assertions.assertEquals(firstName,
                identityPage.getFirstName(),
                "The effective first name is not the expected one");
        Assertions.assertEquals(lastName,
                identityPage.getLastName(),
                "The effective last name is not the expected one");
        Assertions.assertEquals(email,
                identityPage.getEmail(),
                "The effective email is not the expected one");
        Assertions.assertEquals(LocalDate.parse(birthDate, DATE_FORMATTER),
                identityPage.getBirthDate(),
                "The effective birth date is not the expected one");
        Assertions.assertEquals(StringsHelper.convertYesNoIntoBoolean(acceptPartnerOffers),
                identityPage.doesAcceptPartnerOffers(),
                "The effective acceptPartnerOffers is not the expected one");
        Assertions.assertEquals(StringsHelper.convertYesNoIntoBoolean(acceptPrivacyPolicy),
                identityPage.doesAcceptPrivacyPolicy(),
                "The effective acceptPrivacyPolicy is not the expected one");
        Assertions.assertEquals(StringsHelper.convertYesNoIntoBoolean(acceptNewsletter),
                identityPage.doesAcceptNewsletter(),
                "The effective acceptNewsletter is not the expected one");
        Assertions.assertEquals(StringsHelper.convertYesNoIntoBoolean(acceptGdpr),
                identityPage.doesAcceptGdpr(),
                "The effective acceptGdpr is not the expected one");
    }

    /**
     * Verify that an error message is displayed
     * @param field Field where we get the error message
     */
    @Then("An error message should be associated with the field {string}")
    public void assertFieldErrorMessageIsDisplayed(final String field) {
        final AccountCreationPageObject accountCreationPageObject = new AccountCreationPageObject();
        accountCreationPageObject.getErrorMessage(field);

    }

    /**
     * Verify that the given message is currently displayed as an error message
     * @param errorMessage Expected error message
     */
    @Then("The error message {string} should be displayed")
    public void assertErrorMessageIsDisplayed(final String errorMessage) {
        final List<String> errorMessages = PageObjectBase.getErrorMessages();
        final String concatenatedErrorMessages = "\"" + String.join("\",\"", errorMessages) + "\"";
        final String formattedListOfErrorMessages = errorMessage.isEmpty() ? "none"
                                                                           : concatenatedErrorMessages;
        final String failedAssertDescription = "Error message \"" + errorMessage + "\" is not in list of current error messages: " + formattedListOfErrorMessages;
        Assertions.assertTrue(errorMessages.contains(errorMessage), failedAssertDescription);
    }
    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ASSERT-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑//
}