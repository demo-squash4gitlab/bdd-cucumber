package prestashoptest.stepdefinitions;

import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import org.junit.jupiter.api.Assertions;
import prestashoptest.pageobjects.CatalogPageObject;
import prestashoptest.pageobjects.HeaderObject;
import prestashoptest.pageobjects.ProductPageObject;

public class ProductStepDefinitions {

    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ACT-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-//

    /**
     * Go to a category
     * the current page must be the home page
     * @param category
     */
    @When("I navigate to category {string}")
    public void navigateToCategory(final String category) {
        final HeaderObject headerObject = new HeaderObject();
        headerObject.selectCategory(category);
        final CatalogPageObject catalogPage = new CatalogPageObject();
        catalogPage.assertIsCurrent();
    }

    /**
     * Go to a product
     * the current page must be the catalog page
     * @param product
     */
    @When("I navigate to product {string}")
    public void navigateToProduct(final String product) {
        final CatalogPageObject catalogPage = new CatalogPageObject();
        catalogPage.assertIsCurrent();
        catalogPage.selectProduct(product);
        final ProductPageObject productPage = new ProductPageObject();
        productPage.assertIsCurrent();
    }

    /**
     * Add the current product to cart <br>
     * the current page must be the product page
     */
    @When("I add to cart")
    public void addToCart() {
        final ProductPageObject productPage = new ProductPageObject();
        productPage.assertIsCurrent();
        productPage.addToCart();
    }

    /**
     * Add customized message
     */
    @When("I customize with message {string}")
    @When("I customize with message") // necessary to support doc strings
    public void customizeWithMessage(final String message) {
        final ProductPageObject productPage = new ProductPageObject();
        productPage.addMessage(message);
    }

    /**
     * Update the quantity to order
     * @param quantity
     */
    @When("I update quantity to {string}")
    public void updateQuantity(final String quantity) {
        final ProductPageObject productPage = new ProductPageObject();
        productPage.fillQuantity(Integer.parseInt(quantity));
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ACT-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-//
    //↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-ASSERT-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓-↓//

    /**
     * Verify the custom message
     * @param message expected message
     */
    @Then("The customized message should be")
    public void assertCustomizedMessageDisplay(final String message) {
        final ProductPageObject productPage = new ProductPageObject();
        Assertions.assertEquals(message, productPage.getCustomMessage(),
                "The customized message displayed is not the expected one");
    }

    /**
     * Verify the warning message
     * @param expectWarningMessage
     */
    @Then("The warning message {string} should be displayed")
    public void assertWarningMessageDisplay(final String expectWarningMessage) {
        final ProductPageObject productPage = new ProductPageObject();
        productPage.assertIsCurrent();
        Assertions.assertEquals(expectWarningMessage, productPage.getQuantityWarningMessage(),
                "The warning message displayed is not the expected one");
    }

    /**
     * Verify that product cannot be added to cart
     */
    @Then("I should not be able to add to cart")
    public void assertInabilityToAddToCart() {
        final ProductPageObject productPage = new ProductPageObject();
        Assertions.assertFalse(productPage.isAddToCartPossible(), "The product can be added to cart");
    }

    //↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-ASSERT-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑-↑//
}