package prestashoptest.seleniumtools;

import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Assertions;
import org.openqa.selenium.*;

/**
 * Methods used by all pages excepts Header methods
 */
public class PageObjectBase extends ComponentObjectBase {

    private static final DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern("dd/MM/yyyy");
    private final String url;
    private final Pattern urlRegex;

    /**
     * Constructor for pages having a constant URL
     *
     * @param url URL of the page
     */
    protected PageObjectBase(final String url) {
        this.url = url;
        this.urlRegex = null;
    }

    /**
     * Constructor for pages not having a constant URL
     *
     * @param urlRegex Regular expression defining the format of the page URL
     */
    protected PageObjectBase(final Pattern urlRegex) {
        this.url = null;
        this.urlRegex = urlRegex;
    }

    private boolean hasConstantUrl() {
        return (this.url != null);
    }

    private boolean currentUrlIsExpected(final String currentUrl) {
        if (hasConstantUrl()) {
            return currentUrl.equals(getHost() + "/" + this.url);
        }
        if (!currentUrl.startsWith(getHost() + "/")) return false;
        final String pageUrlComponent = currentUrl.substring(getHost().length() + 1);
        final Matcher matcher = this.urlRegex.matcher(pageUrlComponent);
        return matcher.matches();
    }

    /**
     * Go to the page
     * The page must have a constant URL, otherwise the test will crash
     */
    public void goTo() {
        if (!hasConstantUrl()) {
            throw new IllegalStateException("The current page object does not support goTo (it has no constant URL)");
        }
        getDriver().get(getHost() + "/" + this.url);
    }

    /**
     * Assert that the page is currently displayed
     */
    public void assertIsCurrent() {
        final String currentUrl = getDriver().getCurrentUrl();
        Assertions.assertTrue(currentUrlIsExpected(currentUrl),
                "current URL (" + currentUrl
                        + ") does not match the expected one (" + getHost() + "/"
                        + (hasConstantUrl() ? this.url : this.urlRegex) + ")");
    }

    /**
     * Return a screenshot
     * If the driver is not initialized, return null.
     *
     * @return screenshot screenshot
     */
    public static byte[] getScreenshot() {
        if (!isDriverProperlyInitialized()) return null;
        return ((TakesScreenshot) getDriver()).getScreenshotAs(OutputType.BYTES);
    }

    /**
     * Get all error messages currently displayed
     * This is specific to Prestashop: we get the texts of the elements having the "alert-danger" class
     *
     * @return list of the error messages
     */
    public static List<String> getErrorMessages() {
        final Selector dangerMessagesLocator = new Selector(SelectBy.CLASS, "alert-danger");
        return dangerMessagesLocator.findElements()
                .stream()
                .map(e -> e.getText())
                .collect(Collectors.toList());
    }

    /**
     * Gets the DateTimeFormat defined in PageObjectsBase.
     *
     * @return The DateTimeFormatter used to format dates.
     */
    protected static DateTimeFormatter getDateFormatter() {
        return DATE_FORMATTER;
    }
}