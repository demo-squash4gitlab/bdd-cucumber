package prestashoptest.seleniumtools;

import java.time.Duration;
import java.util.List;
import java.util.function.Function;
import java.util.function.Supplier;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 * Base class for all UI objects (page objects and other ones)
 */
public class ComponentObjectBase {
    private static final Duration TIMEOUT = Duration.ofSeconds(10);
    private static Browser browser;
    private static String host;

    /**
     * Initialize the host (i.e. the base of the URL, e.g. "https://example.com/")
     * This must be done, otherwise the test will crash
     *
     * @param host host
     */
    public static void setHost(final String host) {
        ComponentObjectBase.host = host;
    }

    /**
     * Return the host
     * 
     * @return host
     */
    protected static String getHost() {
        return ComponentObjectBase.host;
    }

    /**
     * Select the browser to use for running the test
     * This must be done, otherwise the test will crash
     *
     * @param browser browser
     */
    public static void setBrowser(final Browser browser) {
        ComponentObjectBase.browser = browser;
    }

    /**
     * Return the WebDriver to use, creating one it there is no driver has already been created yet
     *
     * @return WebDriver
     */
    public static WebDriver getDriver() {
        return ComponentObjectBase.browser.getDriver();
    }

    /**
     * Test if there is currently a WebDriver (there can be none if it has not been created yet of if its creation has failed)
     *
     * @return true is there is currently a WebDriver
     */
    public static boolean isDriverProperlyInitialized() {
        return  (ComponentObjectBase.browser != null) && ComponentObjectBase.browser.isDriverProperlyInitialized();
    }

    /**
     * Close the browser
     */
    public static void quit() {
        if (ComponentObjectBase.browser != null) {
            ComponentObjectBase.browser.quit();
            ComponentObjectBase.browser = null;
        }
        ComponentObjectBase.host = null;
    }

    /**
     * Return the element matching the selector
     *
     * @param selector element selector
     * @return matching element
     */
    protected WebElement getElement(final Selector selector) {
        return selector.findElement();
    }

    /**
     * Return the list of elements matching the selector
     *
     * @param selector element selector
     * @return list of matching elements
     */
    protected List<WebElement> getElements(final Selector selector) {
        return selector.findElements();
    }

    /**
     * Get an attribute of the element
     *
     * @param selector element selector
     * @param attribute attribute
     * @return value of the element's attribute
     */
    protected String getElementAttribute(final Selector selector,
                                         final String attribute) {
        return selector.findElement().getAttribute(attribute);
    }

    /**
     * Get the text of the element
     *
     * @param selector element selector
     * @return text
     */
    protected String getElementText(final Selector selector) {
        return selector.findElement().getText();
    }

    /**
     * Get the text of the element without the text of its sub-elements
     *
     * @param selector element selector
     * @return text
     */
    protected String getElementOnlyText(final Selector selector) {
        final String text = selector.findElement().getAttribute("innerHTML");
        return text.replaceAll("<[^>]*>.*</[^>]*>", "") // remove the HTML nodes from the string
                   .replaceAll("<[^>]*/>", "");
    }

    /**
     * Get the value of an element
     *
     * @param selector element selector
     * @return value
     */
    protected String getFieldValue(final Selector selector) {
        return getElementAttribute(selector, "value");
    }

    /**
     * Fill an element (that element must be a field in a form)
     *
     * @param selector element selector
     * @param value value
     */
    protected void fillFieldValue(final Selector selector,
                                  final String value) {
        final WebElement element = selector.findElement();
        element.sendKeys(Keys.CONTROL + "a");
        element.sendKeys(Keys.DELETE);
        element.sendKeys(value);
    }

    /**
     * Selects an entry based on the given selector and string.
     *
     * @param selector the selector used to locate the element
     * @param string   the visible text of the entry to select
     */
    protected void selectEntry(final Selector selector, final String string) {
        clickElement(selector);
        final Select select = new Select(selector.findElement());
        select.selectByVisibleText(string);
    }

    /**
     * Get the status of an element (that element must be a checkbox)
     *
     * @param selector element selector
     * @return status of the checkbox
     */
    protected boolean isCheckBoxSelected(final Selector selector) {
        return selector.findElement().isSelected();
    }

    /**
     * Get the status of the button (clickable or not)
     *
     * @param selector element selector
     * @return true is clickable, false otherwise
     */
    protected boolean isButtonClickable(final Selector selector) {
        return selector.findElement().isEnabled();
    }

    /**
     * Hover over an element
     *
     * @param selector element selector
     */
    protected void hoverElement(final Selector selector) {
        final Actions actions = new Actions(getDriver());
        final WebElement element = selector.findElement();
        actions.moveToElement(element).perform();
    }

    /**
     * Wait for the element to be visible
     *
     * @param selector element selector
     */
    protected void waitElementIsVisible(final Selector selector) {
        final WebDriverWait wait = new WebDriverWait(getDriver(), TIMEOUT);
        wait.until(ExpectedConditions.visibilityOfElementLocated(selector.apply()));
    }

    /**
     * Wait for the element to be invisible
     *
     * @param selector element selector
     */
    protected void waitElementIsInvisible(final Selector selector) {
        final WebDriverWait wait = new WebDriverWait(getDriver(), TIMEOUT);
        wait.until(ExpectedConditions.invisibilityOfElementLocated(selector.apply()));
    }

    /**
     * Wait for the element to be clickable
     *
     * @param selector element selector
     */
    protected void waitElementIsClickable(final Selector selector) {
        final WebDriverWait wait = new WebDriverWait(getDriver(), TIMEOUT);
        wait.until(ExpectedConditions.elementToBeClickable(selector.apply()));
    }

    /**
     * Click on an element
     *
     * @param selector element selector
     */
    protected void clickElement(final Selector selector) {
        selector.findElement().click();
    }

    /**
     * Helper to simplify the definitions of the Selenium selectors
     */
    public enum SelectBy {

        ID(s -> new By.ById(s)),
        NAME(s -> new By.ByName(s)),
        XPATH(s -> new By.ByXPath(s)),
        CLASS(s -> new By.ByClassName(s)),
        LINKTEXT(s -> new By.ByLinkText(s)),
        LINKTEXTEXTRACT(s -> new By.ByPartialLinkText(s)),
        CSS(s -> new By.ByCssSelector(s));

        private final Function<String, By> selectorBy;

        SelectBy(final Function<String, By> selectorBy) {
            this.selectorBy = selectorBy;
        }

        private By apply(final String key) {
            return this.selectorBy.apply(key);
        }
    }

    /**
     * Helper to simplify the use of WebDrivers
     */
    public enum Browser {

        CHROME(() -> {
            final ChromeOptions chromeOptions = new ChromeOptions();
            chromeOptions.addArguments("--no-sandbox",
                                       "--disable-dev-shm-usage",
                                       "--disable-gpu",
                                       "--headless");
            chromeOptions.setBinary("/usr/bin/chromium");
            return new ChromeDriver(chromeOptions);
        }),
        FIREFOX(() -> {
            final FirefoxOptions firefoxOptions = new FirefoxOptions();
            firefoxOptions.addArguments("-headless",
                                        "-safe-mode");
            return new FirefoxDriver(firefoxOptions);
        });

        private final Supplier<WebDriver> driverBuilder;
        private WebDriver driver;
        private boolean isDriverProperlyInitialized = false;

        Browser(final Supplier<WebDriver> driverBuilder) {
            this.driverBuilder = driverBuilder;
        }

        private WebDriver getDriver() {
            // we perform a lazy creation of the driver so the tester does not need all possible drivers on his/her machine, s/he just needs the ones s/he uses
            if (this.driver == null) {
                this.driver = this.driverBuilder.get();
                // the next line is executed only if the WebDriver is properly initialized
                this.isDriverProperlyInitialized = true;
            }
            return this.driver;
        }

        private boolean isDriverProperlyInitialized() {
            return this.isDriverProperlyInitialized;
        }

        private void quit() {
            if (this.driver != null) {
                if (this.isDriverProperlyInitialized) {
                    this.driver.quit();
                    this.isDriverProperlyInitialized = false;
                }
                this.driver = null;
            }
        }
    }

    /**
     * Selenium selector
     */
    public static class Selector {

        private final SelectBy by;
        private final String key;

        public Selector(final SelectBy by, final String key) {
            this.by = by;
            this.key = key;
        }

        private By apply() {
            return this.by.apply(this.key);
        }

        protected WebElement findElement() {
            return getDriver().findElement(apply());
        }

        protected List<WebElement> findElements() {
            return getDriver().findElements(apply());
        }
    }
}