package prestashoptest.datatypes;

/**
 * POJO for the gender of a customer
 */
public enum Gender {

    MALE("M"),
    FEMALE("F"),
    UNDEFINED("U");

    private final String code;

    Gender(final String code) {
        this.code = code;
    }

    public static Gender ofCode(final String code) {
        for (final Gender gender : Gender.values()) {
            if (gender.code.equals(code)) {
                return gender;
            }
        }
        throw new IllegalArgumentException("\"" + code + "\" is not a Gender code");
    }
}
