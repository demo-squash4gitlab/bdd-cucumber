package prestashoptest.datatypes;
/**
 * POJO for a customer
 */
public class Customer {

    private final String firstName;
    private final String lastName;
    private final String email;
    private final String password;

    public Customer(final String firstName,
                    final String lastName,
                    final String email,
                    final String password) {
        this.firstName= firstName;
        this.lastName= lastName;
        this.email = email;
        this.password = password;
    }

    public String getFirstName() {
        return this.firstName;
    }

    public String getLastName() {
        return this.lastName;
    }

    public String getEmail() {
        return this.email;
    }

    public String getPassword() {
        return this.password;
    }
}