package prestashoptest.helpers;

import java.text.NumberFormat;
import java.text.ParseException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class FloatsHelper {

    private static final Pattern PRICE_PATTERN = Pattern.compile("(\\d+,\\d+)\\s€");

    /**
     * Convert a string into a float using the default locale
     *
     * @param str string
     * @return float value
     * @throws ParseException the string is not formatted correctly
     */
    public static float convertStringToFloat(final String str) throws ParseException {
        final NumberFormat format = NumberFormat.getInstance();
        return format.parse(str).floatValue();
    }

    /**
     * Convert a string representing a price (expressed in euros) into a float using the default locale
     *
     * @param str price string
     * @return float value
     * @throws ParseException the price string is not formatted correctly
     */
    public static float convertPriceStringToFloat(final String str) throws ParseException {
        final Matcher matcher = PRICE_PATTERN.matcher(str);
        if (!matcher.find()) {
            throw new ParseException("The price is not properly formatted: \"" + str + "\"", 0);
        }
        return convertStringToFloat(matcher.group(1));
    }
}
