package prestashoptest.helpers;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.junit.jupiter.api.Assertions;

import prestashoptest.datatypes.CartAndOrderLine;

/**
 * Assertion on the content of a cart or an order
 */
public class OrderContentVerification {

    /**
     * Convert a list of Map&lt;String, String&gt; (representing the rows of a data table) into a list of CartAndOrderLine.<br>
     * The "Product" and "Number" columns are required.<br>
     * The "Customization", "Dimension", "Size", "Color", "UnitPrice", and "TotalProductPrice" columns are optional. If such
     * a column is missing, the corresponding value of the CartAndOrderLine is set to null.
     *
     * @param cartData Date table
     * @return extracted list of CartAndOrderLine
     */
    public static List<CartAndOrderLine> extractCartAndOrderLines(final List<Map<String, String>> cartData) {
        final List<CartAndOrderLine> dataLines = new ArrayList<>();
        for (final Map<String, String> cartRow : cartData) {
            final String product = cartRow.get("Product");
            final int quantity = Integer.parseInt(cartRow.get("Number"));
            final String customization = cartRow.get("Customization");
            final String dimension = cartRow.get("Dimension");
            final String size = cartRow.get("Size");
            final String color = cartRow.get("Color");
            Float productPrice = null;
            final String productPriceString = cartRow.get("UnitPrice");
            if (productPriceString != null) {
                try {
                    productPrice = FloatsHelper.convertStringToFloat(productPriceString);
                } catch (final ParseException e) {
                    Assertions.fail("UnitPrice is not properly formatted: " + e.getMessage());
                }
            }
            Float totalProductPrice = null;
            final String totalProductPriceString = cartRow.get("TotalProductPrice");
            if (totalProductPriceString != null) {
                try {
                    totalProductPrice = FloatsHelper.convertStringToFloat(totalProductPriceString);
                } catch (final ParseException e) {
                    Assertions.fail("TotalProductPrice is not properly formatted: " + e.getMessage());
                }
            }
            final CartAndOrderLine dataLine = new CartAndOrderLine(product, quantity, dimension, size, color, customization, productPrice, totalProductPrice);
            dataLines.add(dataLine);
        }
        return dataLines;
    }

    /**
     * Assert that two lists of CartAndOrderLine are equal
     *
     * @param expectedLines Expected list
     * @param effectiveLines Effective list
     */
    public static void assertEquals(final List<CartAndOrderLine> expectedLines,
                                    final List<CartAndOrderLine> effectiveLines){
        Assertions.assertEquals(expectedLines.size(), effectiveLines.size(), "The number of lines is not the expected one");
        for (int i = 0; i < expectedLines.size(); i++) {
            final CartAndOrderLine expectedLine = expectedLines.get(i);
            final CartAndOrderLine effectiveLine = effectiveLines.get(i);
            Assertions.assertEquals(expectedLine.getProduct(), effectiveLine.getProduct(), "The product name is not the expected one.");
            Assertions.assertEquals(expectedLine.getQuantity(), effectiveLine.getQuantity(), "The quantity is not the expected one.");
            if (expectedLine.getCustomization() != null) {
                Assertions.assertEquals(expectedLine.getCustomization(), effectiveLine.getCustomization(), "The product name is not the expected one.");
            }
            if (expectedLine.getDimension() != null) {
                Assertions.assertEquals(expectedLine.getDimension(), effectiveLine.getDimension(), "The dimension is not the expected one.");
            }
            if (expectedLine.getSize() != null) {
                Assertions.assertEquals(expectedLine.getSize(), effectiveLine.getSize(), "The size is not the expected one.");
            }
            if (expectedLine.getColor() != null) {
                Assertions.assertEquals(expectedLine.getColor(), effectiveLine.getColor(), "The color is not the expected one.");
            }
            if (expectedLine.getProductPrice() != null) {
                Assertions.assertEquals(expectedLine.getProductPrice(), effectiveLine.getProductPrice(), "The price is not the expected one.");
            }
            if (expectedLine.getTotalProductPrice() != null) {
                Assertions.assertEquals(expectedLine.getTotalProductPrice(), effectiveLine.getTotalProductPrice(), "The total price is not the expected one.");
            }
        }
    }
}