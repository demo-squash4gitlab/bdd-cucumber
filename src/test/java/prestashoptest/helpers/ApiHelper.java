package prestashoptest.helpers;

import org.apache.commons.text.StringEscapeUtils;
import org.junit.jupiter.api.Assertions;
import org.opentestfactory.exception.ParameterException;
import org.opentestfactory.util.ParameterService;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import prestashoptest.datatypes.CartAndOrderLine;
import prestashoptest.datatypes.Gender;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Base64;
import java.util.List;

public class ApiHelper {

    private static String url;
    private static Logger logger;

    /**
     * Set the URL of the PrestaShop site to which API calls will be made
     *
     * @param url URL of the PrestaShop site
     */
    public static void setUrl(final String url) {
        ApiHelper.url = url;
    }

    /**
     * Set the logger used by this class
     *
     * @param logger logger
     */
    public static void setLogger(final Logger logger) {
        ApiHelper.logger = logger;
    }

    /**
     * Create a customer
     *
     * @param gender     gender
     * @param firstName  firstName
     * @param lastName   lastName
     * @param email      email
     * @param password   password
     * @param birthDate  birthDate
     * @param offers     offers
     * @param newsletter newsletter
     */
    public static void createCustomer(final Gender gender,
                                      final String firstName,
                                      final String lastName,
                                      final String email,
                                      final String password,
                                      final LocalDate birthDate,
                                      final boolean offers,
                                      final boolean newsletter) {
        try {
            final URL u = new URI(url + "/api/customers").toURL();
            // Convert data to put them directly in the XML payload
            final String offersIdConverted = convertBooleanIntoStringXML(offers);
            final String formattedDate = birthDate.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
            final String newsletterConverted = convertBooleanIntoStringXML(newsletter);
            final String formattedGender = convertGenderIntoStringXML(gender);

            final String xmlData = "<prestashop xmlns:xlink=\"http://www.w3.org/1999/xlink\">\n" +
                    "    <customers>\n" +
                    "        <id_default_group>3</id_default_group>\n" +
                    "        <id_gender>" + StringEscapeUtils.escapeXml11(formattedGender) + "</id_gender>\n" +
                    "        <firstname>" + StringEscapeUtils.escapeXml11(firstName) + "</firstname>\n" +
                    "        <lastname>" + StringEscapeUtils.escapeXml11(lastName) + "</lastname>\n" +
                    "        <email>" + StringEscapeUtils.escapeXml11(email) + "</email>\n" +
                    "        <passwd>" + StringEscapeUtils.escapeXml11(password) + "</passwd>\n" +
                    "        <birthday>" + StringEscapeUtils.escapeXml11(formattedDate) + "</birthday>\n" +
                    "        <optin>" + StringEscapeUtils.escapeXml11(offersIdConverted) + "</optin>\n" +
                    "        <newsletter>" + StringEscapeUtils.escapeXml11(newsletterConverted) + "</newsletter>\n" +
                    "        <active>1</active>\n" +
                    "        <id_shop>1</id_shop>\n" +
                    "    </customers>\n" +
                    "</prestashop>";
            apiRequest("POST", u, xmlData);
            logger.log("INFO: Customer " + email + " has been correctly created");
        } catch (final Exception e) {
            Assertions.fail("An exception occurred: " + getExceptionDescription(e));
        }
    }

    /**
     * Delete a customer
     *
     * @param email Email of the Customer to be deleted
     */
    public static void deleteCustomer(final String email) {
        deleteItems(email, ItemType.CUSTOMER);
    }

    /**
     * Delete an order
     *
     * @param reference Reference of the order to be deleted
     */
    public static void deleteOrder(final String reference) {
        deleteItems(reference, ItemType.ORDER);
    }

    /**
     * Set up the session cart for the customer connected to the session.<br>
     * ! The user must reload the web page to see the cart update !
     *
     * @param cartLines     cart lines to be added to the cart of the customer
     * @param customerEmail Email of the customer related to the session cart
     */
    public static void setupSessionCartForCustomer(final List<CartAndOrderLine> cartLines,
                                                   final String customerEmail) {
        try {
            final Integer sessionCartID = getSessionGuestIdForCustomer(customerEmail);
            if (sessionCartID == null) {
                Assertions.fail("Can't get session cart ID");
            }
            final Integer cartID = getSessionCartIdForGuest(sessionCartID);
            if (cartID == null) {
                Assertions.fail("Can't get cart ID");
            }
            final HttpURLConnection response = apiRequest("GET", new URI(url + "/api/" + ItemType.CART_GUEST.items + "/" + cartID).toURL(), "");
            if (response == null) {
                Assertions.fail("Can't get cart");
            }
            final String xmlBody = buildPayloadToSetProductsInCart(cartLines, response);
            if (xmlBody == null) {
                Assertions.fail("Can't add products to cart");
            }
            apiRequest("PUT", new URI(url + "/api/" + ItemType.CART_GUEST.items + "/" + cartID).toURL(), xmlBody);
        } catch (final URISyntaxException | MalformedURLException e) {
            Assertions.fail("An exception occurred: " + getExceptionDescription(e));
        }
    }

    /**
     * Delete the selected item based on two pieces of information "code" (Email for the customer , Order reference for the order)
     * and the type "order" or "customer" from the Itemtype
     *
     * @param code Input here the data of the object you want to delete.
     * @param type Currently we only handle "order" and "customer" type.
     */
    private static void deleteItems(final String code,
                                    final ItemType type) {
        if (code == null) {
            Assertions.fail(type.filter + " is null , Cannot delete " + type.item);
        }
        try {
            final Integer id = searchItemId(type, code);
            if (id == null) {
                logger.log("INFO: No " + type.item + " was deleted");
                return;
            }
            final URL u = new URI(url + "/api/" + type.items + "/" + id).toURL();
            apiRequest("DELETE", u, "");
            logger.log("INFO: The " + type.item + " " + code + " has been successfully deleted");
        } catch (final Exception e) {
            Assertions.fail("An exception occurred: " + getExceptionDescription(e));
        }
    }
    /**
     * Get the API key from Squash, add a ":" (Prestashop need it) and convert it to Base64
     *
     * @return Base64 API key
     */
    private static String getBase64ApiKey() throws ParameterException {
        final String apiKey = ParameterService.INSTANCE.getString("IT_CUF_presta_api_key");
        final String originalInput = apiKey + ":";
        return Base64.getEncoder().encodeToString(originalInput.getBytes());
    }

    /**
     * Convert a Gender enum into a String
     *
     * @param gender gender to be converted
     * @return gender as a string
     */
    private static String convertGenderIntoStringXML(final Gender gender) {
        return (gender == Gender.MALE) ? "1" : "2";
    }

    /**
     * Convert a boolean into a String
     *
     * @param yesOrNo boolean to be converted
     * @return boolean as a string
     */
    private static String convertBooleanIntoStringXML(final boolean yesOrNo) {
        return (yesOrNo) ? "1" : "0";
    }

    /**
     * Search for an item ID by the item type (enum ItemType), and the value of the item (for example: test@test.fr).<br>
     * - For CUSTOMER, the attribute is the email.<br>
     * - For ORDER, the attribute is the reference.<br>
     * <p>
     * Return a String representing the selected item's ID.
     * The item must be unique because this method only picks the first item in the response.
     *
     * @param itemType  type of the searched item
     * @param attribute attribute value of the searched item
     * @return item ID
     */
    private static Integer searchItemId(final ItemType itemType,
                                        final String attribute) throws URISyntaxException, MalformedURLException {
        final URL u = new URI(url + "/api/" + itemType.items + "?filter[" + itemType.filter + "]=[" + URLEncoder.encode(attribute, StandardCharsets.UTF_8) + "]").toURL();
        final HttpURLConnection connection = apiRequest("GET", u, "");
        try {
            if (connection != null && connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                //XML parse
                final DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                final DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                try (final InputStream stream = connection.getInputStream()) {
                    final Document doc = dBuilder.parse(stream);
                    // XML node selection by the itemType.item , for example the node: customer
                    final NodeList nodeList = doc.getElementsByTagName(itemType.item);
                    if (nodeList.getLength() == 1) {
                        final Element customerElement = (Element) nodeList.item(0);
                        return Integer.valueOf(customerElement.getAttribute("id"));
                    }
                }
            }
        } catch (final Exception e) {
            Assertions.fail("An exception occurred: " + getExceptionDescription(e));
        }
        logger.log("WARN: The " + itemType.item + " " + attribute + " was not found, please check that the " + itemType.item + " exists");
        return null;
    }

    /**
     * Make an API request, send it, and assert the response code.
     *
     * @param method  POST, GET, PUT or DELETE
     * @param u       URL
     * @param xmlBody XML payload
     * @return HttpURLConnection
     */
    private static HttpURLConnection apiRequest(final String method,
                                                final URL u,
                                                final String xmlBody) {
        try {
            final HttpURLConnection connection = (HttpURLConnection) u.openConnection();
            connection.setRequestMethod(method);
            connection.setRequestProperty("Authorization", "Basic " + getBase64ApiKey());
            if (method.equals("POST") || method.equals("PUT")) {
                connection.setRequestProperty("Content-Type", "application/xml");
                connection.setDoOutput(true);
                try (final OutputStream os = connection.getOutputStream()) {
                    os.write(xmlBody.getBytes());
                    os.flush();
                }
            }
            final int responseCode = connection.getResponseCode();
            if (responseCode == HttpURLConnection.HTTP_OK || responseCode == HttpURLConnection.HTTP_CREATED) {
                return connection;
            }
            final StringBuilder response = new StringBuilder();
            try (final BufferedReader reader = new BufferedReader(new InputStreamReader(connection.getErrorStream()))) {
                String line;
                while ((line = reader.readLine()) != null) {
                    response.append(line);
                }
            }
            Assertions.fail("Bad request status =  " + responseCode + " " + response);
            return connection;
        } catch (final Exception e) {
            Assertions.fail("An exception occurred: " + getExceptionDescription(e));
        }
        return null;
    }

    private static String getExceptionDescription(final Throwable e) {
        final StringWriter sw = new StringWriter();
        final PrintWriter pw = new PrintWriter(sw);
        e.printStackTrace(pw);
        return sw.toString();
    }

    /**
     * Get the session guest ID for a customer
     *
     * @param email Email of the customer
     * @return Session guest ID
     */
    private static Integer getSessionGuestIdForCustomer(final String email) {
        try {
            final Integer customerID = searchItemId(ItemType.CUSTOMER, email);
            if (customerID == null) {
                Assertions.fail("The customer " + email + " was not found");
            }
            return searchItemId(ItemType.GUEST, String.valueOf(customerID));
        } catch (final MalformedURLException | URISyntaxException e) {
            Assertions.fail("An exception occurred: " + getExceptionDescription(e));
        }
        // never called, just for allow compilation
        return null;
    }

    /**
     * Get the session cart ID for a guest
     *
     * @param guestID Guest ID
     * @return Session cart ID
     */
    private static Integer getSessionCartIdForGuest(final int guestID) {
        try {
            return searchItemId(ItemType.CART_GUEST, String.valueOf(guestID));
        } catch (final MalformedURLException | URISyntaxException e) {
            Assertions.fail("An exception occurred: " + getExceptionDescription(e));
        }
        // never called, just for allow compilation
        return null;
    }

    /**
     * Return the XML payload to set the content of the cart.<br>
     * Here an extract example of the XML payload:<br>
     *
     * <pre>
     *     {@code
     * <cart_rows nodeType="cart_row" virtualEntity="true">
     *     <cart_row>
     *         <id_product>6</id_product>
     *         <quantity>5</quantity>
     *     </cart_row>
     *     <cart_row>
     *         <id_product>12</id_product>
     *         <quantity>1</quantity>
     *     </cart_row>
     * </cart_rows>
     * }
     * </pre>
     * <p>
     * We build the XML payload by modifying the XML describing the initial state of the cart.
     *
     * @param cartLines           cart lines to be set in the cart
     * @param connectionToGetCart HTTP connection to get the XML describing the initial state of the cart
     */
    private static String buildPayloadToSetProductsInCart(final List<CartAndOrderLine> cartLines,
                                                          final HttpURLConnection connectionToGetCart) {

        try (final InputStream stream = connectionToGetCart.getInputStream()) {
            final DocumentBuilder docBuilder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            final Document doc = docBuilder.parse(stream);
            final Node cartRowsNode = doc.getElementsByTagName("cart_rows").item(0);

            // empty the cart
            removeChildrenSubnodes(cartRowsNode);

            // add the cart lines
            for (final CartAndOrderLine cartline : cartLines) {
                final Element cartRow = doc.createElement("cart_row");
                final Integer productID = searchItemId(ItemType.PRODUCT, String.valueOf(cartline.getProduct()));
                if (productID == null) {
                    Assertions.fail("Can't find the product " + cartline.getProduct());
                }
                addChildNodeToNodeWithText(cartRow, "id_product", String.valueOf(productID));
                addChildNodeToNodeWithText(cartRow, "quantity", Integer.toString(cartline.getQuantity()));
                cartRowsNode.appendChild(cartRow);
            }

            // Convert XML document into XML string
            final Transformer transformer = TransformerFactory.newInstance().newTransformer();
            final StringWriter writer = new StringWriter();
            transformer.transform(new DOMSource(doc), new StreamResult(writer));
            return writer.toString();
        } catch (final ParserConfigurationException | IOException | SAXException | TransformerException |
                       URISyntaxException e) {
            Assertions.fail("An exception occurred: " + getExceptionDescription(e));
            return null;
        }
    }

    /**
     * Remove all children nodes of a node
     *
     * @param node node
     */
    private static void removeChildrenSubnodes(final Node node) {
        final NodeList childNodes = node.getChildNodes();
        for (int i = childNodes.getLength() - 1; i >= 0; i--) {
            final Node childNode = childNodes.item(i);
            node.removeChild(childNode);
        }
    }

    /**
     * Add a child element to an XML element
     *
     * @param parent       Parent element
     * @param childName    Name of the added child element
     * @param childRawText Text content of the added child element (this text shall be raw, it will be escaped)
     */
    private static void addChildNodeToNodeWithText(final Element parent,
                                                   final String childName,
                                                   final String childRawText) {
        final Element child = parent.getOwnerDocument().createElement(childName);
        child.setTextContent(StringEscapeUtils.escapeXml11(childRawText));
        parent.appendChild(child);
    }

    /**
     * Possible types of items which are used in this class
     */
    private enum ItemType {

        CUSTOMER("customers", "customer", "email"),
        ORDER("orders", "order", "reference"),
        GUEST("guests", "guest", "id_customer"),
        CART_GUEST("carts", "cart", "id_guest"),
        PRODUCT("products", "product", "name");

        private final String items;
        private final String item;
        private final String filter;

        ItemType(final String items,
                 final String item,
                 final String filter) {
            this.items = items;
            this.item = item;
            this.filter = filter;
        }
    }
}
