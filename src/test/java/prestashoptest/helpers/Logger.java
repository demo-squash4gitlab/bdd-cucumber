package prestashoptest.helpers;

/**
 * Logger
 */
public class Logger {

    private final StringBuilder builder;
    private boolean inSetup;
    private boolean inTeardown;
    private Integer stepNumber;
    private int stepCount;

    /**
     * Constructor
     */
    public Logger() {
        this.builder = new StringBuilder();
    }

    /**
     * Logs that the setup is starting
     */
    public void logSetupStart() {
        if (this.inSetup) {
            throw new IllegalStateException("logSetupStart cannot be called while already in setup");
        }
        if (this.stepNumber != null) {
            throw new IllegalStateException("logSetupStart cannot be called while in a step");
        }
        if (this.inTeardown) {
            throw new IllegalStateException("logSetupStart cannot be called while in teardown");
        }
        this.inSetup = true;
    }

    /**
     * Logs that the teardown is ending
     */
    public void logSetupEnd() {
        if (!this.inSetup) {
            throw new IllegalStateException("logSetupEnd cannot be called while not in setup");
        }
        this.inSetup = false;
    }

    /**
     * Logs that the teardown is starting
     */
    public void logTeardownStart() {
        if (this.inSetup) {
            throw new IllegalStateException("logTeardownStart cannot be called while in setup");
        }
        if (this.stepNumber != null) {
            throw new IllegalStateException("logTeardownStart cannot be called while in a step");
        }
        if (this.inTeardown) {
            throw new IllegalStateException("logTeardownStart cannot be called while already in teardown");
        }
        this.inTeardown = true;
    }

    /**
     * Logs that the Teardown is ending
     */
    public void logTeardownEnd() {
        if (!this.inTeardown) {
            throw new IllegalStateException("logSTeardownEnd cannot be called while not in teardown");
        }
        this.inTeardown = false;
    }

    /**
     * Logs that a step is starting
     */
    public void logStepStart() {
        if (this.inSetup) {
            throw new IllegalStateException("logStepStart cannot be called while in setup");
        }
        if (this.stepNumber != null) {
            throw new IllegalStateException("logStepStart cannot be called while already in a step");
        }
        if (this.inTeardown) {
            throw new IllegalStateException("logStepStart cannot be called while in teardown");
        }
        this.stepCount++;
        this.stepNumber = Integer.valueOf(this.stepCount);
    }

    /**
     * Logs that a step is ending
     */
    public void logStepEnd() {
        if (this.stepNumber == null) {
            throw new IllegalStateException("logStepEnd cannot be called while not in a step");
        }
        this.stepNumber = null;
    }

    /**
     * Logs a message
     *
     * @param message
     */
    public void log(final String message) {
        if (this.inSetup) {
            this.builder.append("Setup - ");
        }
        if (this.stepNumber != null) {
            this.builder.append("Step ").append(this.stepNumber).append(" - ");
        }
        if (this.inTeardown) {
            this.builder.append("Teardown - ");
        }
        this.builder.append(message).append("\n");
    }

    /**
     * Get the logs
     *
     * @return logs
     */
    public String getLogs() {
        return this.builder.toString();
    }
}
