package prestashoptest.helpers;

import java.util.Random;

/**
 * Helpers for managing strings
 */
public class StringsHelper {

    /**
     * Convert "Yes" into true, "No" into false
     * Other string values will generate an IllegalArgumentException exception
     *
     * @param str string to convert
     * @return boolean
     */
    public static boolean convertYesNoIntoBoolean(final String str) {
        if (str.equals("yes")) return true;
        if (str.equals("no")) return false;
        throw new IllegalArgumentException("Cannot convert \"" + str + "\" into a boolean, the string should be \"yes\" or \"no\".");
    }

    /**
     * Generate a random string of 16 characters (in the interval [a-z])
     *
     * @return generated string
     */
    public static String generateRandomId() {
        final int LENGTH = 16;
        final Random random = new Random();
        final StringBuilder builder = new StringBuilder(LENGTH + 1);
        for (int i = 0; i < LENGTH; i++) {
            final int r = random.nextInt(25);
            builder.append((char) ('a' + r));
        }
        return builder.toString();
    }
}
