# BDD + Cucumber automation example

This repository contains an example of a **Squash test automation project**.  
This project tests a typical e-commerce site ([PrestaShop]( https://prestashop.com/)).  
It is based on a Squash TM project using **[BDD test cases]( https://tm-en.doc.squashtest.com/latest/user-guide/manage-test-cases/conceive-bdd-script.html) and Cucumber**.  
(It is an evolution of the project used in the [Using BDD and Cucumber in Squash](https://autom-devops-en.doc.squashtest.com/latest/howto/bdd%2Bcucumber/index.html) HowTo.)


# Usage

These tests are designed to be run from Squash TM, they are using CUFs (custom fields) and datasets. 

# Versions

The tested SUT is:

- Prestashop demo version 1.7.8.7

The tests are using the following libraries:

- Cucumber 7.14.0
- JUnit 5.10.0
- Selenium 4.15.0

# Organisation of the code
The code applies the typical Maven / Cucumber organisation.
- src:
    - test
        - java
            - prestashoptest
                - RunCucumberTest.java
                - SetupTeardown.java
                - datatypes *business entities*
                    - CartAndOrderLine.java
                    - Customer.java
                    - Gender.java
                - helpers *miscellaneous helpers*
                    - ApiHelper.java
                    - FloatsHelper.java
                    - Logger.java
                    - OrderContentVerification.java
                    - StringsHelper.java
                - pageobjects *Page Objects*
                    - AccountCreationPageObject.java
                    - CartPageObject.java
                    - CatalogPageObject.java
                    - HeaderObject.java
                    - HomePageObject.java
                    - IdentityPageObject.java
                    - OrderPageObject.java
                    - ProductPageObject.java
                    - SignInPageObject.java
                - seleniumtools *Selenium helpers*
                    - ComponentObjectBase.java
                    - PageObjectBase.java
                - stepdefinitions *implementation of the Cucumber steps*
                    - AccountStepDefinitions.java
                    - CartStepDefinitions.java
                    - CommonStepDefinitions.java
                    - OrderStepDefinitions.java
                    - ProductStepDefinitions.java
        - resources *feature (Cucumber) files*
            - Account_management
                - Account_creation
                    - 10_Cannot_create_an_account_when_the_email_format_is_invalid.feature
                    - 11_Cannot_create_an_account_when_the_password_is_too_short.feature
                    - 12_Cannot_create_an_account_when_privacy_and_or_GDPR_are_not_checked.feature
                    - 13_Create_an_account.feature
                - Account_modification
                    - 14_Cannot_modify_the_account_information_when_privacy_and_or_GDPR_are_not_checked.feature
                    - 15_Modify_the_account_information.feature
                - Login
                    - 16_Log_in_with_correct_credentials.feature
                    - 17_Cannot_log_in_with_an_incorrect_password.feature
            - Cart_management
                - Add_to_cart
                    - 18_Add_one_product_to_the_cart.feature
                    - 19_Add_one_product_with_a_customized_message_to_the_cart.feature
                    - 20_Add_two_products_to_the_cart.feature
                    - 21_Cannot_add_a_product_to_the_cart_if_the_quantity_exceeds_stock.feature
                    - 22_Cannot_write_a_customized_message_if_the_message_is_too_long.feature
                - Update_cart
                    - 23_Update_the_product_quantity_in_the_cart.feature
                    - 24_Remove_a_product_from_the_cart.feature
                - Order_management
                    - 25_Cannot_place_an_order_if_the_sale_conditions_are_not_approved.feature
                    - 26_Place_an_order_-_logged-in_customer.feature
                    - 27_Place_an_order_-_logged-out_customer.feature
